const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
	// attributes
	username: {
		type: String,
		unique: [true, 'username tidak tersedia'],
		required: true
	},
	email: {
		type: String,
		unique: [true, 'email tidak tersedia'],
		required: false
	},
	password: {
		type: String,
		required: true
	},
	isAdmin: {
		type: Boolean,
		required: false
	},
  _idProfile: {
    type: mongoose.Schema.Types.ObjectId,
    required: false
  }
})
const User = mongoose.model('UsersAccounts', UserSchema)

module.exports = {
	User
}
