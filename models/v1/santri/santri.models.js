const mongoose = require('mongoose');

const SantriSchema = new mongoose.Schema({
	//attributes
	firstname: {
		type: String,
		min: 3,
		required: true,
	},
	lastname: {
		type: String,
		required: false
	},
  dateofbirth: {
    type: Date,
    required: true
  },
	gender: {
		type: String,
		enum: ['male', 'female'],
		required: true
	},
  rolebased: {
    type: String,
    default: "santri"
  },
  _idParents: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Parents',
    required: false
  }
});

const Santri = mongoose.model('Santri', SantriSchema);

module.exports = {
	Santri,
}
