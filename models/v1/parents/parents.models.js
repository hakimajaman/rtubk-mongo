const mongoose = require('mongoose');

const ParentsSchema = new mongoose.Schema({
	//attributes
	firstname: {
		type: String,
		min: 3,
		required: true,
	},
	lastname: {
		type: String,
		required: false
	},
	gender: {
		type: String,
		enum: ['male', 'female'],
		required: true
	},
  rolebased: {
    type: String,
    default: "parents"
  },
  _idUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UsersAccounts',
    required: false
  },
  _idAnak: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Santri',
    required: false
  }]
});

const Parents = mongoose.model('Parents', ParentsSchema);

module.exports = {
	Parents,
}
