const mongoose = require('mongoose');

const ArticleSchema = new mongoose.Schema({
	//attributes
	firstname: {
		type: String,
		min: 3,
		required: true,
	},
	lastname: {
		type: String,
		required: false
	},
	dateofbirth: {
		type: Date,
		required: true
	},
	gender: {
		type: String,
		enum: ['male', 'female'],
		required: true
	},
	rolebased: {
		type: String,
		default: "asatidz"
	},
	asatidz: {
		type: String,
		required: false
	},
  _idUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UsersAccounts',
    required: false
  }
});

const Article = mongoose.model('Article', ArticleSchema);

module.exports = {
	Article,
}
