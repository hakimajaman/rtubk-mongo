const mongoose = require('mongoose');

const InfoSchema = new mongoose.Schema({
	//attributes
	title: {
		type: String,
		min: 3,
		required: true,
	},
	pict: {
		type: String,
		required: false
	},
  body: {
    type: String,
    min: 3,
    required: true
  },
	datepost: {
		type: Date,
    default: Date.now,
		required: false
	},
  number: {
    type: String,
    default: '0',
    enum: ['0','1','2','3'],
    required: true
  },
  _idUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UsersAccounts',
    required: false
  }
});

const Info = mongoose.model('Info', InfoSchema);

module.exports = {
	Info,
}
