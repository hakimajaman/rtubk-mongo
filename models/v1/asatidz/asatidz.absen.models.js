const mongoose = require('mongoose');

const AsatidzAbsenSchema = new mongoose.Schema({
    //attributes
    absen: {
      type: Date,
      default: Date.now,
      required: false
    },
    hasConfirmed: {
      type: Boolean,
      default: false,
      required: false
    },
    _idAsatidz: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Asatidz',
      required: false
    }
});

const AsatidzAbsen = mongoose.model('AsatidzAbsen', AsatidzAbsenSchema);

module.exports = {
  AsatidzAbsen
}
