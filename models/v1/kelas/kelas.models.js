const mongoose = require('mongoose');
//using timestamp for absent
const KelasSchema = new mongoose.Schema({
    //attributes
  namakelas: {
    type: String,
    required: true
  },
  jamkelas: {
    type: String,
    required: true
  },
  _idAsatidz: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Asatidz',
    required: false
  },
  _idSantri: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Santri',
    required: false
  }]
});

const Kelas = mongoose.model('Kelas', KelasSchema);

module.exports = {
  Kelas,
}
