const express = require('express');
const router = express.Router();

const UsersAccountControllers = require('../../controllers/v1/usersAccount.controllers');

router.post('/register', UsersAccountControllers.RegisterUser);
router.post('/login', UsersAccountControllers.LoginUser);

router.get('/getAll', UsersAccountControllers.GetAllUsers);
router.delete('/delete/:id', UsersAccountControllers.DeleteUser);

module.exports = router;
