const express = require('express');
const router = express.Router();
const { isAuth } = require('../../../middleware/v1/auth');
const { isAdmin } = require('../../../middleware/v1/isAdmin');
const { 
    RegisterProfile,
    MyProfile,
    UpdateProfile,
    GetAllProfile,
    UpdateProfileByAdmin,
    DeleteProfileAndAccount, } = require('../../../controllers/v1/santri/santri.controllers');

//Santri Controllers
router.post('/register', isAuth, RegisterProfile);
router.get('/profile/:id', isAuth, MyProfile);
router.put('/update/:id', isAuth, UpdateProfile);
router.put('/update/admin/:id', isAuth, isAdmin, UpdateProfileByAdmin);
router.get('/getAll', isAuth, isAdmin, GetAllProfile);
router.delete('/delete/:id', isAuth, isAdmin, DeleteProfileAndAccount);

module.exports = router;
