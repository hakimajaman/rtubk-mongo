const express = require('express');
const router = express.Router();
const { isAuth } = require('../../../middleware/v1/auth');
const { isAdmin } = require('../../../middleware/v1/isAdmin');
const { 
    PostAbsen,
    ConfirmAbsen,
    //MyProfile,
    //UpdateProfile,
    //GetAllProfile,
    //UpdateProfileByAdmin,
    //DeleteProfileAndAccount,
} = require('../../../controllers/v1/asatidz/asatidz.absen.controllers');

//Asatidz Absen Controllers
router.post('/post', isAuth, PostAbsen);
router.post('/confirm/:id', isAuth, isAdmin, ConfirmAbsen);
//router.get('/profile', isAuth, MyProfile);
//router.put('/update', isAuth, UpdateProfile);
//router.put('/update/:id', isAuth, isAdmin, UpdateProfileByAdmin);
//router.get('/getAll', isAuth, isAdmin, GetAllProfile);
//router.delete('/delete/:id', isAuth, isAdmin, DeleteProfileAndAccount);

module.exports = router;
