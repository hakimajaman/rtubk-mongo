const express = require('express');
const router = express.Router();
const { isAuth } = require('../../../middleware/v1/auth');
const { isAdmin } = require('../../../middleware/v1/isAdmin');
const { 
    RegisterKelas,
    MyClass,
    UpdateKelas,
    AddSantriToKelas,
    MoveSantriToKelas,
    GetAllKelas,
    //UpdateProfileByAdmin,
    //DeleteProfileAndAccount, 
  } = require('../../../controllers/v1/kelas/kelas.controllers');

//Kelas Controllers
router.post('/register', isAuth, RegisterKelas);
router.get('/ruang', isAuth, MyClass);
router.put('/update/:id', isAuth, isAdmin, UpdateKelas);
router.put('/ruang/update/:id', isAuth, AddSantriToKelas);
router.put('/ruang/pindah/:from/:to', isAuth, MoveSantriToKelas);
router.get('/getAll', isAuth, GetAllKelas);
//router.delete('/delete/:id', isAuth, isAdmin, DeleteProfileAndAccount);

module.exports = router;
