const express = require('express');
const router = express.Router();
const { isAuth } = require('../../../middleware/v1/auth');
const { isAdmin } = require('../../../middleware/v1/isAdmin');
const { 
    PostInfo,
    //MyProfile,
    //UpdateProfile,
    //GetAllProfile,
    //UpdateProfileByAdmin,
    //DeleteProfileAndAccount,
  } = require('../../../controllers/v1/article/info.controllers');

//Asatidz Controllers
router.post('/post', isAuth, PostInfo);
//router.get('/profile', isAuth, MyProfile);
//router.put('/update', isAuth, UpdateProfile);
//router.put('/update/:id', isAuth, isAdmin, UpdateProfileByAdmin);
//router.get('/getAll', isAuth, isAdmin, GetAllProfile);
//router.delete('/delete/:id', isAuth, isAdmin, DeleteProfileAndAccount);

module.exports = router;
