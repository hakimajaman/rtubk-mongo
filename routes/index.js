var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//routeV1-UsersAccount
const UsersAccountRouter = require('./v1/usersAccount.routes');
router.use('/next', UsersAccountRouter);

//routeV1-Asatidz
const AsatidzRouter = require('./v1/asatidz/asatidz.routes');
const AsatidzAbsenRouter = require('./v1/asatidz/asatidz.absen.routes');
router.use('/asatidz', AsatidzRouter);
router.use('/asatidz/absen', AsatidzAbsenRouter);

//routeV1-Parents
const ParentsRouter = require('./v1/parents/parents.routes');
router.use('/parents', ParentsRouter);

//routeV1-Santri
const SantriRouter = require('./v1/santri/santri.routes');
router.use('/santri', SantriRouter);

//routeV1-Kelas
const KelasRouter = require('./v1/kelas/kelas.routes');
router.use('/kelas', KelasRouter);

//routeV1-Article
//const ArticleRouter = require('./v1/article/article.routes');
const ArticleInfoRouter = require('./v1/article/info.routes');
//router.use('/article', ArticleRouter);
router.use('/info', ArticleInfoRouter);

module.exports = router;
