const { User } = require('../../models/v1/usersAccount.models');

let isAdmin = async(req, res, next) => {
    try{
      var findit = await User.findById(req.user.id);
      if(findit.isAdmin == false){
        return res.status(401).json("You are not admin");
      } else if(findit.isAdmin == true){
        next();
      }
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

module.exports = {
    isAdmin
}
