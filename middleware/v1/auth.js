const jwt = require('jsonwebtoken');
const { User } = require('../../models/v1/usersAccount.models');
require('dotenv').config();

let isAuth = async(req, res, next) => {
	var token = req.header("Authorization");
	if(!token){
		return res.status(401).json("Token Needed");
	}
	try{
		let decoded = jwt.verify(token, process.env.SECRET_KEY);
		req.user = decoded;
		let findUser = await User.findById(req.user.id);
		if(!findUser){
			return res.status(404).json("No User Accounts");
		}
		next();
	}
	catch(err){
		return res.status(422).json("Wrong Data Token");
	}
}

module.exports = {
	isAuth,
}
