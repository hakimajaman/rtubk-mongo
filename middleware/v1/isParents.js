const { User } = require('../../models/v1/usersAccount.models');
const { Parents } = require('../../models/v1/parents/parents.models');
const { Santri } = require('../../models/v1/santri/santri.models');

let isParents = async(req, res, next) => {
    try{
      var findAccount = await User.findById(req.user.id);
      var findSantri = await Santri.findById(req.params.id);

      var IDCompare = {
        IDParentProfile: findAccount._idProfile,
        IDParentSantri: findSantri._idParents
      }

      console.log(IDCompare.IDParentSantri);
      console.log(IDCompare.IDParentProfile);

      if(IDCompare.IDParentSantri !== IDCompare.IDParentProfile){
        return res.status(401).json("This not your kid profile");
      } else if(IDCompare.IDParentProfile === IDCompare.IDParentSantri){
        next();
      }
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

module.exports = {
  isParents,
}
