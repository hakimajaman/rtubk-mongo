const { User } = require('../../models/v1/usersAccount.models');

let ifExists = async(req, res, next) => {
    try{
      var findit = await User.findById(req.user.id);
      if(findit._idProfile){
        return res.status(401).json("You Have Been Registered");
      } else if(findit._idProfile === undefined){
        next();
      }
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

module.exports = {
  ifExists,
}
