const mongoose = require('mongoose');
require('dotenv').config();

const configDB = {
	development: process.env.DBDEV
}

mongoose.connect(configDB.development, {
	useNewUrlParser: true,
	useFindAndModify: false,
	useUnifiedTopology: true
}).then(() => console.log('Connect to Mongo Database'));
