const { Parents } = require('../../../models/v1/parents/parents.models');
const { User } = require('../../../models/v1/usersAccount.models.js');
const { Santri } = require('../../../models/v1/santri/santri.models');

let RegisterProfile = async(req, res) => {
	try{
    var findParent = await User.findById(req.user.id);
		var post = await Santri.create({
			firstname: req.body.Firstname,
			lastname: req.body.Lastname,
      dateofbirth: req.body.Date,
			gender: req.body.Gender,
      _idParents: findParent._idProfile
		});
    var findIdParentAndUpdate = await Parents.findByIdAndUpdate(findParent._idProfile,{
      $push: { _idAnak: post.id }
    });
    var getit = await Santri.findById(post.id)
        .populate({
            path: '_idParents'
        });
		return res.status(200).json(getit);
	}
	catch(err){
		return res.status(400).json(err);
	}
}

let MyProfile = async(req, res) => {
    try{
      var findSantri = await Santri.findById(req.params.id);
      return res.status(200).json(findSantri);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let UpdateProfile = async(req, res) => {
    try{
      var updateit = await Santri.findByIdAndUpdate(req.params.id,{
        $set: (req.body)
      }); 
      var showit = await Santri.findById(req.params.id);
      return res.status(200).json(showit);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let UpdateProfileByAdmin = async(req, res) => {
    try{
      var findit = await Santri.findByIdAndUpdate(req.params.id, {
        $set: (req.body)
      });
      var showit = await Santri.findById(req.params.id);
      return res.status(200).json(showit);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let GetAllProfile = async(req, res) => {
    try{
      var findAll = await Santri.find();
      return res.status(200).json(findAll);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let DeleteProfileAndAccount = async(req, res) => {
    try{
      var findProfile = await Santri.findById(req.params.id).populate({
          path: '_idParents'
      });

      var userdeleted = {
        'Firstname': findProfile.firstname,
        'Lastname': findProfile.lastname,
        'gender': findProfile.gender,
        'dateofbirth': findProfile.dateofbirth,
        'parents': findProfile._idParents.firstname + " " + findProfile._idParents.lastname
      }

      var findProfileInParentAndDelete = await Parents.findByIdAndUpdate(findProfile._idParents, {
          $pullAll: {_idAnak: [req.params.id]}
      });
      var deleteProfile = await Santri.findByIdAndDelete(req.params.id);
      
      var showit = await Santri.find();
      return res.status(200).json({
        "Userdeleted": userdeleted,
        "All Users": showit
      });
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

module.exports = {
	RegisterProfile,
  MyProfile,
  UpdateProfile,
  UpdateProfileByAdmin,
  GetAllProfile,
  DeleteProfileAndAccount,
}
