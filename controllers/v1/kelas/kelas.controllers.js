const { Asatidz } = require('../../../models/v1/asatidz/asatidz.models');
const { User } = require('../../../models/v1/usersAccount.models');
const { Kelas } = require('../../../models/v1/kelas/kelas.models');

let RegisterKelas = async(req, res) => {
    try{
      var post = await Kelas.create({
        namakelas: req.body.namakelas,
        jamkelas: req.body.jamkelas,
        _idAsatidz: req.body._idAsatidz,
        _idSantri: req.body._idSantri
      });

      return res.status(201).json(post);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let MyClass = async(req, res) => {
    try{
      var findUser = await User.findById(req.user.id);
      var findit = await Kelas.find({ _idAsatidz: findUser._idProfile });
      return res.status(200).json(findit);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let UpdateKelas = async(req, res) => {
    try{
      var updateit = await Kelas.findByIdAndUpdate(req.params.id, {
          $set: (req.body)
      });
      var showit = await Kelas.findById(req.params.id);
      return res.status(200).json(showit);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let AddSantriToKelas = async(req, res) => {
    try{
      var addtokelas = await Kelas.findByIdAndUpdate(req.params.id, {
          $push: {
            _idSantri: req.body._idSantri
          }
      });
      var showit = await Kelas.findById(req.params.id);
      return res.status(200).json(showit);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let MoveSantriToKelas = async(req, res) => {
    try{
      var removefromkelas = await Kelas.findByIdAndUpdate(req.params.from, {
        $pullAll: {_idSantri: [req.body._idSantri]}
      });
      var movetokelas = await Kelas.findByIdAndUpdate(req.params.to, {
        $push: {
          _idSantri: req.body._idSantri
        }
      });
      var findfromkelas = await Kelas.findById(req.params.from);
      var findtokelas = await Kelas.findById(req.params.to);
      return res.status(200).json({
        "Kelas Sebelumnya": findfromkelas,
        "Kelas Sekarang": findtokelas
      });
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let GetAllKelas = async(req, res) => {
    try{
      var findAll = await Kelas.find();
      return res.status(200).json(findAll);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

module.exports = {
  RegisterKelas,
  MyClass,
  UpdateKelas,
  AddSantriToKelas,
  MoveSantriToKelas,
  GetAllKelas
}
