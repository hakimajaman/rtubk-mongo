const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../../models/v1/usersAccount.models');
require('dotenv').config();

let RegisterUser = async(req, res) => {
	try{
		var post = await User.create({
			username: req.body.Username,
			email: req.body.Email,
			password: req.body.Password,
			isAdmin: req.body.isAdmin
		});
		post.password = await bcrypt.hashSync(post.password, 10);
		await post.save();
		return res.status(201).json("Pendaftaran Berhasil");
	}
	catch(err){
		return res.status(400).json(err);
	}
}

let LoginUser = async(req, res) => {
	try{
		var {
			Email, Username, Password
		} = req.body;
		var userExists = await User.findOne({
			username: Username
		});
		if(!userExists){
			return res.status(404).json("Email Tidak Terdaftar");
		}
		var compareit = await bcrypt.compare(
			Password, userExists.password
		);
		if(!compareit){
			return res.status(404).json("Password Salah!");
		}
		const token = await jwt.sign({
      id: userExists.id }, process.env.SECRET_KEY, { expiresIn: '2h'}
		);
		return res.status(200).json(token);
	}
	catch(err){
		return res.status(400).json(err);
	}
}

let GetAllUsers = async(req, res) => {
	try{
		var findIt = await User.find();
		return res.status(200).json(findIt);
	}
	catch(err){
		return res.status(400).json(err);
	}
}

let DeleteUser = async(req, res) => {
	try{
		var deleteIt = await User.findByIdAndDelete(req.params.id);
		return res.status(200).json("Berhasil diDelete!");
	}
	catch(err){
		return res.status(400).json(err);
	}
}

module.exports = {
	RegisterUser,
	LoginUser,

	GetAllUsers,
	DeleteUser,
}
