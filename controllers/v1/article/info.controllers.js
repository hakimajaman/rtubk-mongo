const { User } = require('../../../models/v1/usersAccount.models.js');
const { Info } = require('../../../models/v1/article/info.models');

let PostInfo = async(req, res) => {
	try{
		var post = await Info.create({
			title: req.body.title,
      body: req.body.body,
      number: req.body.numbers,
      _idUser: req.user.id
		});
    var getit = await Info.findById(post.id)
        .populate({
            path: '_idUser'
        });
		return res.status(200).json(getit);
	}
	catch(err){
		return res.status(400).json(err);
	}
}

//let MyProfile = async(req, res) => {
    //try{
      //var findUser = await User.findById(req.user.id);
      //var findProfile = await Asatidz.findById(findUser._idProfile);
      //return res.status(200).json(findProfile);
    //}
    //catch(err){
      //console.log(err);
      //return res.status(400).json(err);
    //}
//}

//let UpdateProfile = async(req, res) => {
    //try{
      //var findit = await User.findById(req.user.id);
      //var updateit = await Asatidz.findByIdAndUpdate(findit._idProfile,{
        //$set: (req.body)
      //}); 
      //var showit = await Asatidz.findById(findit._idProfile);
      //return res.status(200).json(showit);
    //}
    //catch(err){
      //console.log(err);
      //return res.status(400).json(err);
    //}
//}

//let UpdateProfileByAdmin = async(req, res) => {
    //try{
      //var findit = await Asatidz.findByIdAndUpdate(req.params.id, {
        //$set: (req.body)
      //});
      //var showit = await Asatidz.findById(req.params.id);
      //return res.status(200).json(showit);
    //}
    //catch(err){
      //console.log(err);
      //return res.status(400).json(err);
    //}
//}

//let GetAllProfile = async(req, res) => {
    //try{
      //var findAll = await Asatidz.find();
      //return res.status(200).json(findAll);
    //}
    //catch(err){
      //console.log(err);
      //return res.status(400).json(err);
    //}
//}

//let DeleteProfileAndAccount = async(req, res) => {
    //try{
      //var findProfile = await Asatidz.findById(req.params.id);

      //var userdeleted = {
        //'Firstname': findProfile.firstname,
        //'Lastname': findProfile.lastname,
        //'gender': findProfile.gender,
        //'username': findProfile._idUser.username,
      //}

      //var findAccountAndDelete = await User.findByIdAndDelete(findProfile._idUser);
      //var deleteProfile = await Asatidz.findByIdAndDelete(req.params.id);
      
      //var showit = await Asatidz.find();
      //return res.status(200).json({
        //"Userdeleted": userdeleted,
        //"All Users": showit
      //});
    //}
    //catch(err){
      //console.log(err);
      //return res.status(400).json(err);
    //}
//}

module.exports = {
  PostInfo,
  //MyProfile,
  //UpdateProfile,
  //UpdateProfileByAdmin,
  //GetAllProfile,
  //DeleteProfileAndAccount,
}
