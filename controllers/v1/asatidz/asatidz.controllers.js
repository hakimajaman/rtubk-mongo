const { User } = require('../../../models/v1/usersAccount.models.js');
const { Asatidz } = require('../../../models/v1/asatidz/asatidz.models');

let RegisterProfile = async(req, res) => {
	try{
		var post = await Asatidz.create({
			firstname: req.body.Firstname,
			lastname: req.body.Lastname,
			dateofbirth: req.body.Date,
			gender: req.body.Gender,
      _idUser: req.user.id
		});
    var findIdUserAndUpdate = await User.findByIdAndUpdate(req.user.id,{
      $set: { _idProfile: post.id }
    });
    var getit = await Asatidz.findById(post.id)
        .populate({
            path: '_idUser'
        });
		return res.status(200).json(getit);
	}
	catch(err){
		return res.status(400).json(err);
	}
}

let MyProfile = async(req, res) => {
    try{
      var findUser = await User.findById(req.user.id);
      var findProfile = await Asatidz.findById(findUser._idProfile);
      return res.status(200).json(findProfile);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let UpdateProfile = async(req, res) => {
    try{
      var findit = await User.findById(req.user.id);
      var updateit = await Asatidz.findByIdAndUpdate(findit._idProfile,{
        $set: (req.body)
      }); 
      var showit = await Asatidz.findById(findit._idProfile);
      return res.status(200).json(showit);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let UpdateProfileByAdmin = async(req, res) => {
    try{
      var findit = await Asatidz.findByIdAndUpdate(req.params.id, {
        $set: (req.body)
      });
      var showit = await Asatidz.findById(req.params.id);
      return res.status(200).json(showit);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let GetAllProfile = async(req, res) => {
    try{
      var findAll = await Asatidz.find();
      return res.status(200).json(findAll);
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let DeleteProfileAndAccount = async(req, res) => {
    try{
      var findProfile = await Asatidz.findById(req.params.id);

      var userdeleted = {
        'Firstname': findProfile.firstname,
        'Lastname': findProfile.lastname,
        'gender': findProfile.gender,
        'username': findProfile._idUser.username,
      }

      var findAccountAndDelete = await User.findByIdAndDelete(findProfile._idUser);
      var deleteProfile = await Asatidz.findByIdAndDelete(req.params.id);
      
      var showit = await Asatidz.find();
      return res.status(200).json({
        "Userdeleted": userdeleted,
        "All Users": showit
      });
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

module.exports = {
	RegisterProfile,
  MyProfile,
  UpdateProfile,
  UpdateProfileByAdmin,
  GetAllProfile,
  DeleteProfileAndAccount,
}
