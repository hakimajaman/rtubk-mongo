const { User } = require('../../../models/v1/usersAccount.models');
const { AsatidzAbsen } = require('../../../models/v1/asatidz/asatidz.absen.models');

let PostAbsen = async(req, res) => {
    try{
      var findit = await User.findById(req.user.id);
      var post = await AsatidzAbsen.create({
        _idAsatidz: findit._idProfile
      });
      return res.status(200).json(post)
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

let ConfirmAbsen = async(req, res) => {
    try{
      var updateit = await AsatidzAbsen.findById(req.params.id, {
        $set: { hasConfirmed: true }
      });
      return res.status(200).json("Absen Telah diKonfirmasi");
    }
    catch(err){
      console.log(err);
      return res.status(400).json(err);
    }
}

module.exports = {
  PostAbsen,
  ConfirmAbsen,
}
